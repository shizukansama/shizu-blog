##Introduction


##Installation
- Création de la base de données

CREATE DATABASE blog;

- Création de l'utilisateur de base de données

CREATE USER derrierelespages IDENTIFIED BY '***';

- Ajout des droits à l'utilisateur

USE derrierelespages;

GRANT ALL PRIVILEGES ON derrierelespages TO 'derrierelespages'
REVOKE ALL PRIVILEGES ON *.* FROM 'derrierelespages'@'%'; REVOKE GRANT OPTION ON *.* FROM 'derrierelespages'@'%'; GRANT SELECT, INSERT, UPDATE, DELETE, FILE ON *.* TO 'derrierelespages'@'%' ;

- Création des tables de bases de données

CREATE TABLE articles (
    id int NOT NULL AUTO_INCREMENT,
    title varchar(255) NOT NULL,
    content TEXT,
    visibility INT,
    date DATE,
    PRIMARY KEY (id)
);


- Création du dossier de data (pour les images uploadées)

- Complèter le fichier de configuration

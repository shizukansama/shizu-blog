<?php

require_once 'config.php';

require_admin();

$title = "admin";

$page = get_param('page', 'string');

switch ($page) {
    case 'articles' :
        $action    = get_param('action', 'string');
        $articleid = get_param('articleid', 'int');
        switch ($action) {
            case "delete" :
                delete_article($articleid);
                break;
            case "hide":
                edit_article($articleid, ['visibility' => 0]);
                break;
            case "show":
                edit_article($articleid, ['visibility' => 1]);
                break;
        }

        $articles     = get_all_articles();
        $menuselected = 'managearticles';
        $template     = 'admin_articles.php';
        break;
    case 'add-article':
    case 'edit-article':
        $template = 'admin_edit_article.php';

        $editarticleid = get_param('articleid', 'int');
        $article       = false;
        if ($editarticleid) {
            $article = get_article_by_id($editarticleid);
        }

        $articletitle       = get_param('title', 'string');
        $articledescription = get_param('description', 'html');
        $articlecontent     = get_param('content', 'html');
        $articletextid      = get_param('textid', 'string');
        $articledate        = strtotime(get_param('date', 'string'));
        if ($articletextid) {
            $ok = false;
            if (!$editarticleid) {
                $ok = add_article($articletextid, $articletitle, $articlecontent, $articledescription, 0, $articledate);
            } else {
                $ok = edit_article($editarticleid, [
                    'textid'      => $articletextid,
                    'title'       => $articletitle,
                    'content'     => $articlecontent,
                    'description' => $articledescription,
                    'date'        => $articledate
                ]);
            }
            if ($ok) {
                $url = $CFG->siteUrl . '/' . $articletextid;
                redirect($url);
            }
        }
        break;
    case "comments":
        $action    = get_param('action', 'string');
        $commentid = get_param('commentid', 'int');
        switch ($action) {
            case "delete" :
                delete_article_comment($commentid);
                break;
            case "hide":
                edit_article_comment($commentid, ['visibility' => 0]);
                break;
            case "show":
                edit_article_comment($commentid, ['visibility' => 1]);
                break;
        }

        $commentsbyarticles = get_not_valided_comments_by_articles();
        $nbComments         = 0;
        foreach ($commentsbyarticles as $articletextid => $comments) {
            $nbComments += count($comments);
        }
        $menuselected = 'managecomments';
        $template     = 'admin_comments.php';
        break;
    case "pages":
        $action    = get_param('action', 'string');
        $pageid = get_param('pageid', 'int');
        switch ($action) {
            case "delete" :
                delete_page($pageid);
                break;
            case "hide":
                edit_page($pageid, ['visibility' => 0]);
                break;
            case "show":
                edit_page($pageid, ['visibility' => 1]);
                break;
        }

        $pages        = get_all_pages();
        $menuselected = 'managepages';
        $template     = 'admin_pages.php';

        break;
    case 'add-page':
    case 'edit-page':
        $template = 'admin_page_edit.php';

        $editpageid = get_param('pageid', 'int');
        $page       = false;
        if ($editpageid) {
            $page = get_page_by_id($editpageid);
        }

        $pagetextid = get_param('textid', 'string');
        $pagetitle   = get_param('title', 'string');
        $pagecontent = get_param('content', 'html');
        if ($pagetextid) {
            $ok = false;
            if (!$editpageid) {
                $ok = add_page($pagetextid, $pagetitle, $pagecontent, 0);
            } else {
                $ok = edit_page($editpageid, [
                    'textid'      => $pagetextid,
                    'title'       => $pagetitle,
                    'content'     => $pagecontent,
                ]);
            }
            if ($ok) {
                $url = $CFG->siteUrl . '/' . $pagetextid;
                redirect($url);
            }
        }
        break;
    case 'logs':
        $logs     = [];
        $menuselected = 'logs';
        $template     = 'admin_logs.php';
        $logs = get_logs();

        break;
    default:

}

require('templates/admin.php');
<?php

require_once 'config.php';

$textid  = get_param('id', 'string');
$article = null;
if ($textid) {
    $article = get_article_by_textid($textid);
}
if (!$article || (!is_admin() && !$article->is_visible())) {
    $hasothercontent  = true;
    $articles         = get_more_articles();
    $articlelisttitle = 'Accès aux articles qui existent !';
    $templateother    = 'articles_list.php';

    $title = 'Perdu ?';

    $content = '<div class="error-page"><h2>Perdu ?</h2>
            <p>La page que vous recherchez n\'existe pas.</p>
            <p>Mais voici des articles qui pourraient vous intéresser !</p></div>';
    require('templates/base.php');
    die;
}

if ($action = get_param('action', 'string')) {
    switch ($action) {
        // Traitement des commentaires
        case "addcomment":
            $commentcontent  = get_param('add-comment-content', 'string');
            $commentusername = get_param('add-comment-username', 'string');
            $commentemail    = get_param('add-comment-email', 'string');
            $parentcommentid = get_param('add-comment-parentid', 'string');
            if ($commentcontent == '' || $commentusername == '') {
                $errormessage = 'Votre commentaire ne contient pas de nom d\'utilisateur ou pas de contenu.';
            } else {
                $commentid = add_article_comment($article->get_id(), $parentcommentid, $commentusername, $commentcontent, $commentemail);
                $_POST           = [];
                $commentcontent  = '';
                $commentusername = '';
                $commentemail    = '';
                $parentcommentid = 0;
                $successmessage  = 'Merci pour votre commentaire ! Il sera visible après validation (pour éviter les robots)';
                add_log('article.php', 'add', 'article\add_comment', [
                    'articleid' => $article->get_id(),
                    'commentid' => $commentid,
                ]);
            }
            $article = get_article_by_textid($textid);
            break;
    }
}else{
    add_log('article.php', 'view', 'article\view', [
        'articleid' => $article->get_id()
    ]);
}

$menuselected    = $article->get_textid();
$title           = $article->get_title();
$hasothercontent = true;

$articles         = get_more_articles($article->get_id());
$articlelisttitle = 'Aller plus loin :';
$templateother    = 'articles_list.php';

$template = 'article.php';

require('templates/base.php');
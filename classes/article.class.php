<?php

class article {

    private $id;
    private $textid;
    private $title;
    private $content;
    private $description;
    private $visibility;
    private $date;
    private $comments;

    public function __construct($record) {
        $this->id          = $record->id;
        $this->textid      = $record->textid;
        $this->title       = $record->title;
        $this->content     = $record->content;
        $this->description = $record->description;
        $this->visibility  = $record->visibility;
        $this->date        = $record->date;
    }

    public function is_visible() {
        return $this->visibility == 1;
    }

    public function get_id(){
        return $this->id;
    }

    public function get_textid() {
        return $this->textid;
    }

    public function get_title() {
        return $this->title;
    }

    public function get_content() {
        return $this->content;
    }

    public function get_timestamp(){
        return $this->date;
    }

    public function get_date() {
        return date('d/m/Y à H:i', $this->date);
    }

    public function get_description() {
        return $this->description;
    }

    public function get_article_link() {
        global $CFG;
        return $CFG->siteUrl . '/' . $this->textid;
    }

    public function get_comments(){
        return $this->comments;
    }

    public function set_comments($comments){
        $this->comments = $comments;
    }

    public function get_url(){
        global $CFG;
        return $CFG->siteUrl . '/' . $this->get_textid();
    }
}

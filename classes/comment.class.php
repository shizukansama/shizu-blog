<?php

class comment {

    private $id;
    private $articleid;
    private $parentid;
    private $username;
    private $email;
    private $content;
    private $date;
    private $visibility;
    private $children;
    private $ip;

    public function __construct($record) {
        $this->id         = $record->id;
        $this->parentid   = $record->parentid;
        $this->articleid  = $record->articleid;
        $this->content    = $record->content;
        $this->date       = $record->date;
        $this->username   = $record->username;
        $this->visibility = $record->visibility;
        $this->ip         = $record->ip;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_article_id() {
        return $this->articleid;
    }

    public function get_username() {
        return $this->username;
    }

    public function get_content() {
        return $this->content;
    }

    public function get_date() {
        return date('d/m/Y à H:i', $this->date);
    }

    public function get_email() {
        return $this->email;
    }

    public function get_ip() {
        return $this->ip;
    }

    public function is_visible() {
        return $this->visibility == 1 ||$this->get_ip() == $_SERVER['REMOTE_ADDR'];
    }

    public function is_validated(){
        return $this->visibility == 1;
    }

    public function set_children($comments) {
        $this->children = $comments;
    }

    public function get_children() {
        return $this->children;
    }
}

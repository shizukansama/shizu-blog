<?php

require_once(__DIR__ . '/../config.php');
require_once(__DIR__ . '/article.class.php');
require_once(__DIR__ . '/comment.class.php');

class database_interface {
    private $db;

    public function __construct() {
        global $CFG;

        try {
            $this->db = new PDO('mysql:host=' . $CFG->dbHost . ';dbname=' . $CFG->dbName . ';charset=UTF8', $CFG->dbUser, $CFG->dbPass);
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /*** Articles ***/
    public function is_article_textid($textid) {
        $prep = $this->db->prepare("SELECT * FROM articles WHERE textid = :textid");
        $prep->execute(array('textid' => $textid));
        return $result = $prep->fetch();
    }

    public function get_article_by_id($id) {
        $prep = $this->db->prepare("SELECT * FROM articles WHERE id = :id");
        $prep->execute(array('id' => $id));
        $result = $prep->fetch();
        if (!$result) {
            return false;
        }
        return new article((object) $result);
    }

    public function get_article_by_text_id($textid) {
        $prep = $this->db->prepare("SELECT * FROM articles WHERE textid = :textid");
        $prep->execute(array('textid' => $textid));
        $result = $prep->fetch();
        if (!$result) {
            return false;
        }
        return new article((object) $result);
    }

    public function get_article_id_by_textid($textid) {
        $prep = $this->db->prepare("SELECT id FROM articles where textid = :textid");
        $prep->execute(['textid' => $textid]);
        return $prep->fetch()['id'];
    }

    public function get_articles() {
        $results = $this->db->query("SELECT * FROM articles ORDER BY date DESC")->fetchAll();

        $articles = [];
        foreach ($results as $result) {
            $articles[] = new article((object) $result);
        }

        return $articles;
    }

    public function add_article($textid, $title, $content, $description, $visibility, $date = 0) {
        if ($date == 0) {
            $date = time();
        }

        $prep = $this->db->prepare("INSERT INTO articles(textid, title, content, description, visibility, date)
            VALUES (:textid, :title, :content, :description, :visibility, :date)");

        $prep->execute([
            'textid'      => $textid,
            'title'       => $title,
            'description' => $description,
            'content'     => $content,
            'visibility'  => $visibility,
            'date'        => $date,
        ]);
    }

    public function edit_article($articleid, $articledata) {
        $data  = ['id' => $articleid];
        $query = "UPDATE articles SET ";
        foreach ($articledata as $key => $value) {
            if ($value !== '') {
                $query      .= " $key = :$key , ";
                $data[$key] = $value;
            }
        }
        $query = substr($query, 0, -2);
        $query .= " WHERE id = :id";

        $prep = $this->db->prepare($query);
        $prep->execute($data);
    }

    public function delete_article($articleid) {
        $prep = $this->db->prepare("DELETE FROM comments where articleid = :articleid");
        $prep->execute(['articleid' => $articleid]);

        $prep = $this->db->prepare("DELETE FROM articles where id = :id");
        $prep->execute(['id' => $articleid]);
    }

    /*** Commentaires  ***/

    public function get_article_comments($articleid) {
        $prep = $this->db->prepare("SELECT * FROM comments WHERE articleid = :articleid AND parentid = 0 ORDER BY date DESC");
        $prep->execute(['articleid' => $articleid]);
        $results = $prep->fetchAll();

        $comments = [];
        foreach ($results as $result) {
            $comment  = new comment((object) $result);
            $children = $this->get_comment_children($comment->get_id());
            $comment->set_children($children);
            $comments[] = $comment;
        }

        return $comments;
    }

    private function get_comment_children($parentcommentid) {
        $prep = $this->db->prepare("SELECT * FROM comments where parentid = :parentid ORDER BY date DESC");
        $prep->execute(['parentid' => $parentcommentid]);
        $results  = $prep->fetchAll();
        $comments = [];
        foreach ($results as $result) {
            $comment  = new comment((object) $result);
            $children = $this->get_comment_children($comment->get_id());
            $comment->set_children($children);
            $comments[] = $comment;
        }

        return $comments;
    }

    public function get_unvalidated_comments() {
        $prep    = $this->db->query("SELECT * FROM comments where visibility = 0 ORDER BY date ASC");
        $results = $prep->fetchAll();

        $comments = [];
        foreach ($results as $result) {
            $comments[] = new comment((object) $result);
        }
        return $comments;
    }

    public function add_article_comment($articleid, $parentid, $username, $content, $email = '') {
        $date       = time();
        $visibility = 0;
        $ip         = $_SERVER['REMOTE_ADDR'];

        $content = strip_tags($content);
        $content = str_replace("\n", '<br>', $content);

        $prep = $this->db->prepare("INSERT INTO comments(articleid, parentid, username, ip, email, content, date, visibility) 
            VALUES (:articleid, :parentid, :username, :ip, :email, :content, :date, :visibility)");
        $prep->execute([
            'articleid'  => $articleid,
            'parentid'   => $parentid,
            'username'   => $username,
            'ip'         => $ip,
            'email'      => $email,
            'content'    => $content,
            'date'       => $date,
            'visibility' => $visibility
        ]);

        return $this->db->lastInsertId();

    }

    public function edit_article_comment($commentid, $commentdata) {
        $data  = ['id' => $commentid];
        $query = "UPDATE comments SET ";
        foreach ($commentdata as $key => $value) {
            if ($value !== '') {
                $query      .= " $key = :$key , ";
                $data[$key] = $value;
            }
        }
        $query = substr($query, 0, -2);
        $query .= " WHERE id = :id";

        $prep = $this->db->prepare($query);
        $prep->execute($data);
    }

    public function delete_article_comment($commentid) {
        $prep = $this->db->prepare("DELETE FROM comments where id = :id");
        $prep->execute(['id' => $commentid]);
        // TODO supprimer les commentaires affilié ou les replacer au niveau supérieur
    }

    /*** Pages  ***/
    public function is_page_textid($textid) {
        $prep = $this->db->prepare("SELECT * FROM pages WHERE textid = :textid");
        $prep->execute(array('textid' => $textid));
        return $prep->fetch();
    }

    public function get_page_by_id($id) {
        $prep = $this->db->prepare("SELECT * FROM pages WHERE id = :id");
        $prep->execute(array('id' => $id));
        $result = $prep->fetch();
        return new page((object) $result);
    }

    public function get_page_by_textid($textid) {
        $prep = $this->db->prepare("SELECT * FROM pages WHERE textid = :textid");
        $prep->execute(array('textid' => $textid));
        $result = $prep->fetch();
        if (!$result) {
            return false;
        }
        return new page((object) $result);
    }

    public function get_pages() {
        $results = $this->db->query("SELECT * FROM pages")->fetchAll();

        $pages = [];
        foreach ($results as $result) {
            $pages[] = new page((object) $result);
        }

        return $pages;
    }

    public function add_page($textid, $title, $content, $visibility) {

        $prep   = $this->db->prepare("INSERT INTO pages(textid, title, content, visibility)
            VALUES (:textid, :title, :content, :visibility)");
        $result = $prep->execute([
            'textid'     => $textid,
            'title'      => $title,
            'content'    => $content,
            'visibility' => $visibility,
        ]);

        return $result;

    }

    public function edit_page($pageid, $pagedata) {
        $data  = ['id' => $pageid];
        $query = "UPDATE pages SET ";
        foreach ($pagedata as $key => $value) {
            if ($value !== '') {
                $query      .= " $key = :$key , ";
                $data[$key] = $value;
            }
        }
        $query = substr($query, 0, -2);
        $query .= " WHERE id = :id";

        $prep = $this->db->prepare($query);
        return $prep->execute($data);
    }

    public function delete_page($pageid) {
        $prep = $this->db->prepare("DELETE FROM pages where id = :id");
        $prep->execute(['id' => $pageid]);
    }

    /* WIP : Newsletter */
    public function is_in_newsletter($email) {
        $prep = $this->db->prepare("SELECT * FROM newsletter WHERE email = :email");
        $prep->execute(array('email' => $email));
        $result = $prep->fetch();
        return $result ? true : false;
    }

    public function add_email_to_newsletter($email) {
        if ($this->is_in_newsletter($email)) {
            $prep = $this->db->prepare("UPDATE newsletter SET activated=1 WHERE email = :email");
            $prep->execute(array('email' => $email));
        } else {
            $prep = $this->db->prepare("INSERT INTO newsletter(email, dateadded, activated) 
            VALUES (:email, :dateadded, :activated)");
            $prep->execute([
                'email'     => $email,
                'dateadded' => time(),
                'activated' => 1,
            ]);
        }
    }

    public function remove_email_to_newsletter($email) {
        if ($this->is_in_newsletter($email)) {
            $prep = $this->db->prepare("UPDATE newsletter SET activated=0 WHERE email = :email");
            $prep->execute(array('email' => $email));
        }
        return true;
    }

    public function get_unsubscribe_link($email) {

    }

    /* Search */
    public function articles_search($search) {
        $prep = $this->db->prepare("SELECT * FROM articles WHERE title LIKE :search OR description LIKE :search OR content LIKE :search");
        $prep->execute(['search' => '%' . $search . '%']);
        $articlesrecords = $prep->fetchAll();
        if (!$articlesrecords) {
            return [];
        }
        $articlesbyid = [];

        $matcharticles = [];

        $matcharticlesorder = [];
        foreach ($articlesrecords as $article) {
            $matchcounter = 0;

            // Title search
            $title = explode(' ', $article['title']);
            foreach ($title as $part) {
                if ($part == $search) {
                    $matchcounter += 5;
                }
            }

            // Description search
            $description = strip_tags($article['description']);
            $description = explode(' ', $description);
            foreach ($description as $part) {
                if ($part == $search) {
                    $matchcounter += 3;
                }
            }

            // Content search
            $content = strip_tags($article['content']);
            $content = explode(' ', $content);
            foreach ($content as $part) {
                if ($part == $search) {
                    $matchcounter += 0.5;
                }
            }

            if ($matchcounter > 0) {
                $matcharticlesorder[$article['id']] = $matchcounter;
                $articlesbyid[$article['id']]       = $article;
            }
        }
        arsort($matcharticlesorder);

        foreach ($matcharticlesorder as $articleid => $counter) {
            $article         = $articlesbyid[$articleid];
            $matcharticles[] = new article((object) $article);
        }

        return $matcharticles;
    }

    /***************** Word wars *****************/
    public function add_wordwar_session($creator, $ip, $timebegin, $duration) {
        $prep = $this->db->prepare("INSERT INTO wordwar_sessions(creator, ip, timebegin, duration) 
            VALUES (:creator, :ip, :timebegin, :duration)");
        $prep->execute([
            'creator'   => $creator,
            'ip'        => $ip,
            'timebegin' => $timebegin,
            'duration'  => $duration,
        ]);
        return $this->db->lastInsertId();
    }

    public function get_wordwar_session($id) {
        $prep = $this->db->prepare("SELECT * FROM wordwar_sessions WHERE id = :id");
        $prep->execute(['id' => $id]);
        $result = $prep->fetch();
        return $result ? (object) $result : false;
    }

    public function get_open_wordwar_sessions() {
        $now  = strtotime("now");
        $prep = $this->db->prepare("
            SELECT * 
            FROM wordwar_sessions 
            WHERE id IN ( 
                SELECT id 
                FROM wordwar_sessions 
                WHERE timebegin <= :timebegin 
                GROUP BY id 
                HAVING SUM( duration * 60 + timebegin ) > :timebegin2
            )");
        $prep->execute([
            'timebegin'  => $now,
            'timebegin2' => $now
        ]);
        $results = $prep->fetchAll();

        $wordwars = [];
        foreach ($results as $result) {
            $wordwars[] = new wordwar_session((object) $result);
        }
        return $wordwars;
    }

    public function get_wordwar_sessions($mintime) {
        $prep = $this->db->prepare("SELECT * FROM wordwar_sessions WHERE timebegin > :mintime ORDER BY timebegin ASC");
        $prep->execute([
            'mintime' => $mintime
        ]);
        $results = $prep->fetchAll();

        $wordwars = [];
        foreach ($results as $result) {
            $wordwars[] = new wordwar_session((object) $result);
        }
        return $wordwars;
    }

    public function get_wordwar_participant($wwid, $ip, $name) {
        $prep = $this->db->prepare("SELECT * FROM wordwars_participants WHERE wwid = :wwid AND ip = :ip AND name = :name");
        $prep->execute([
            'wwid' => $wwid,
            'ip'   => $ip,
            'name' => $name,
        ]);

        $result = $prep->fetch();
        return $result ? (object) $result : false;
    }

    public function get_wordwar_participant_from_ip($wwid, $ip) {
        $prep = $this->db->prepare("SELECT * FROM wordwars_participants WHERE wwid = :wwid AND ip = :ip");
        $prep->execute([
            'wwid' => $wwid,
            'ip'   => $ip
        ]);

        $result = $prep->fetch();
        return $result ? (object) $result : false;
    }

    public function add_wordwar_participant($wwid, $name, $ip) {
        $prep = $this->db->prepare("INSERT INTO wordwars_participants(wwid, name, ip, timecreated) 
            VALUES (:wwid, :name, :ip, :timecreated)");
        $prep->execute([
            'wwid'        => $wwid,
            'name'        => $name,
            'ip'          => $ip,
            'timecreated' => time(),
        ]);
        return $this->db->lastInsertId();
    }

    public function update_wordwar_participant($id, $name) {
        $prep = $this->db->prepare("UPDATE wordwars_participants SET name=:name WHERE id = :id");
        $prep->execute(array(
            'id'   => $id,
            "name" => $name
        ));
    }

    public function get_wordwar_participants($wwid, $mintime = 0) {
        $prep = $this->db->prepare("SELECT * FROM wordwars_participants WHERE wwid = :wwid AND timecreated >= :mintime ORDER BY timecreated ASC");
        $prep->execute([
            'wwid'    => $wwid,
            'mintime' => $mintime
        ]);
        $participants = $prep->fetchAll();

        return $participants;
    }

    /***************** Logs *****************/
    public function add_log($ip, $page, $type, $eventname, $data, $timecreated) {
        $prep = $this->db->prepare("INSERT INTO logs(ip, page, type, eventname, data, timecreated) 
            VALUES (:ip, :page, :type, :eventname, :data, :timecreated)");

        return $prep->execute([
            'ip'          => $ip,
            'page'        => $page,
            'type'        => $type,
            'eventname'   => $eventname,
            'data'        => json_encode($data),
            'timecreated' => $timecreated,
        ]);
    }

    public function get_logs() {
        $prep = $this->db->prepare("SELECT * FROM logs order by timecreated DESC");
        $prep->execute([

        ]);
        return $prep->fetchAll();
    }
}
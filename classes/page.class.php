<?php

class page {

    private $id;
    private $textid;
    private $title;
    private $content;
    private $visibility;

    public function __construct($record) {
        $this->id          = $record->id;
        $this->textid      = $record->textid;
        $this->title       = $record->title;
        $this->content     = $record->content;
        $this->visibility  = $record->visibility;
    }

    public function is_visible() {
        return $this->visibility == 1;
    }

    public function get_id(){
        return $this->id;
    }

    public function get_textid() {
        return $this->textid;
    }

    public function get_title() {
        return $this->title;
    }

    public function get_content() {
        return $this->content;
    }

    public function get_url(){
        global $CFG;
        return $CFG->siteUrl . '/' . $this->get_textid();
    }
}

<?php

class wordwar_session {

    private $id;
    private $creator;
    private $ip;
    private $timebegin;
    private $duration;

    public function __construct($record) {
        $this->id        = $record->id;
        $this->creator   = $record->creator;
        $this->ip        = $record->ip;
        $this->timebegin = $record->timebegin;
        $this->duration  = $record->duration;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_timebegin_timestamp(){
        return $this->timebegin;
    }

    public function get_timeend_timestamp(){
        return $this->timebegin + $this->duration * 60;
    }

    public function get_timebegin() {
        return date('d/m/Y à H:i', $this->timebegin);
    }

    public function get_endtime(){
        return date('d/m/Y à H:i', ($this->timebegin + $this->duration * 60));
    }

    public function is_done(){
        $now = strtotime("now");
        return $now >= ($this->timebegin + $this->duration * 60);
    }

    public function get_duration(){
        return $this->duration;
    }

    public function get_creator(){
        return $this->creator == '' ? "Anonyme" : $this->creator;
    }

    public function get_url() {
        global $CFG;
        return $CFG->siteUrl . '/wordwars_session.php?wwid=' . $this->get_id();
    }

}
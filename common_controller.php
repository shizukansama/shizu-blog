<?php

require_once 'config.php';

// Ajax


// Redirect to article or page from htaccess
$textid = get_param('redirecttextid', 'string');
if($textid){
    if(is_article_textid($textid)){
        $_GET['id'] = $textid;
        require_once 'article.php';
        die;
    }else if(is_page_textid($textid)){
        $_GET['id'] = $textid;
        require_once 'page.php';
        die;
    } else{
        $title = 'Perdu ?';

        add_log('', 'error', 'error/view', [
            'textid' => $textid
        ]);

        $content = '<div class="error-page"><h2>Perdu ?</h2>
            <p>La page que vous recherchez n\'existe pas.</p>
            <p>Mais voici des articles qui pourraient vous intéresser !</p></div>';
        $articles = get_more_articles();
        $articlelisttitle = 'Aller plus loin :';
        $templateother = 'articles_list.php';
        $hasothercontent = true;
        require('templates/base.php');
        die;
    }
}


// Other action
$action = get_param('action', 'string');
if($action){
    $returnurl = get_param('returnurl', 'string');

    switch ($action){
        case 'subscribenewsletter':
            $email = get_param('email', 'string');
            if($email && count(explode("@", $email)) == 2){
                subscribe_newsletter($email);
            }

            break;
        case 'unsubscribenewsletter':
            $email = get_param('email', 'string');
            $token = get_param('token', 'string');
            if($email && count(explode("@", $email)) == 2){
                unsubscribe_newsletter($email);
            }

            break;

    }
}
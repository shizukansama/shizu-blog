<?php

if(file_exists('local_config.php')){
    require_once 'local_config.php';
}

$CFG = [];

// Version
$CFG['version'] = '1.04';

// Database
$CFG['dbHost'] = isset($_ENV['dbHost']) ? $_ENV['dbHost'] : 'localhost';
$CFG['dbName'] = isset($_ENV['dbName']) ? $_ENV['dbName'] :'root';
$CFG['dbUser'] = isset($_ENV['dbUser']) ? $_ENV['dbUser'] :'root';
$CFG['dbPass'] = isset($_ENV['dbPass']) ? $_ENV['dbPass'] :'root';

// Admin
$CFG['adminLogin'] = isset($_ENV['adminLogin']) ? $_ENV['adminLogin'] : 'admin';
$CFG['adminPassword'] = isset($_ENV['adminPassword']) ? $_ENV['adminPassword'] :'admin';

// Url
$CFG['siteUrl'] = isset($_ENV['siteUrl']) ? $_ENV['siteUrl'] :'http://localhost';

// SMTP
$CFG['emailHost'] = isset($_ENV['emailHost']) ? $_ENV['emailHost'] :'SSL0.OVH.NET';
$CFG['emailPort'] = isset($_ENV['emailPort']) ? $_ENV['emailPort'] : 000;
$CFG['emailUser'] = isset($_ENV['emailUser']) ? $_ENV['emailUser'] :'contact@mon-blog.fr';
$CFG['emailPass'] = isset($_ENV['emailPass']) ? $_ENV['emailPass'] :'ultrapassword';
$CFG['emailDest'] = isset($_ENV['emailDest']) ? $_ENV['emailDest'] :'monemail@mon-blog.fr';

// Social
$CFG['discordUrl'] = 'https://discord.gg/2cT7JShq9d';

$CFG = (object) $CFG;

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();

require_once 'lib.php';
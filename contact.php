<?php

require_once 'config.php';

$contactemail    = get_param('contact-email', 'string');
$contactusername = get_param('contact-username', 'string');
$contactmessage  = get_param('contact-message', 'string');

// Données envoyées
if ($contactemail != '' && $contactusername != '' && $contactmessage != '') {

    // Préparation du contenu
    $message = str_replace("\n", "<br>", $contactmessage);
    $subject = "Formulaire de contact : Message de " . $contactusername ;
    $mailcontent = "Nouveau message envoyé depuis le formulaire de contact.";
    $mailcontent .= "- Date    : " . date('d/m/Y à H:i') . "<br>";
    $mailcontent .= "- Nom     : " . $contactusername . "<br>";
    $mailcontent .= "- Email   : " . $contactemail. "<br>";
    $mailcontent .= "- IP      : " . $_SERVER['REMOTE_ADDR'] . "<br>";
    $mailcontent .= "- Message :<br>" . $message;

    // Envoi
    $error = send_message($CFG->emailDest, $subject, $mailcontent);
    if($error !== true ){
        $errordata = [
            'username' => $contactusername,
            'email' => $contactemail,
            'message' => $contactmessage,
            'destination' => $CFG->emailDest,
            'error' => $error
        ];
        error_log('send mail error with data '. json_encode($errordata));
        $errormessage = 'Erreur lors de l\'envoi du message. Vous pouvez me contacter directement à contact[at]derriere-les-pages.fr (remplacez [at] par @)';
        add_log('contact.php', 'error', 'contact\error_send', []);
    }else{
        $successmessage = 'Merci pour votre message ! Je vous répond dès que possible.';
        $contactemail = '';
        $contactusername = '';
        $contactmessage = '';
        add_log('contact.php', 'send', 'contact\send', []);
    }
}else{
    add_log('contact.php', 'view', 'contact\view', []);
}

$hasothercontent = true;
$menuselected = "contact";
$title = 'Contact';
$template = 'contact.php';
require('templates/base.php');
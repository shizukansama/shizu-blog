<?php

require_once 'config.php';

$content = '<div class="error-page"><h2>Perdu ?</h2>
            <p>La page que vous recherchez n\'existe pas.</p>
            <p>Mais voici des articles qui pourraient vous intéresser !</p></div>';

$title           = "Derrière les pages - Erreur";
$hasothercontent = true;

$articles = get_more_articles();
$articlelisttitle = 'Découvrir';
$templateother = 'articles_list.php';

require('templates/base.php');
<?php

require_once 'config.php';

$search = get_param('search', 'string');

$matchs = [];
if(count_chars($search) > 3){
    $matcharticles = globalsearch_articles_results($search);
    add_log('globalsearch.php', 'view', 'search\view', ['search' => $search]);
}

$title = "Rechercher";
$template = "globalsearch.php";

$hasothercontent = true;
$articles = get_more_articles();
$articlelisttitle = 'Derniers articles';
$templateother = 'articles_list.php';

require('templates/base.php');
<?php

require_once 'config.php';

add_log('index.php', 'view', 'frontpage\view', []);

$menuselected = "frontpage";

$title = 'Derrière les pages';

$articles = get_frontpage_articles();

$template = 'frontpage.php';
$hasothercontent = true;
$othercontent = '<div class="site-description">Décortiquer l’écriture sous toutes ses coutures !</div>';

require('templates/base.php');
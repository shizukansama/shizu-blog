document.addEventListener('DOMContentLoaded', function () {
    init()
}, false);

function init() {
    var that = this;

    // Initialiser la réponse aux commentaires d'articles
    for (let element of document.querySelectorAll(".comment-response")) {
        let commentid = element.dataset.commentid;
        element.onclick = function () {
            document.getElementById("add-comment-parentid").value = commentid;
            let username = document.querySelector('.comment-container[data-commentid="' + commentid + '"] > .comment-user > .comment-name').textContent;
            document.querySelector('#add-comment-container .parent-comment-info').innerHTML = '<span class="remove-comment-parent">X</span>' + "Répondre à " + username;
            location.href = "#add-comment-container";
            that.init_delete_parent_comment();
        };
    }

    // Init rich text editor
    if (document.querySelectorAll(".rich-editor").length > 0) {
        tinymce.init({
            selector: '.rich-editor',
            plugins: [
                'link image code',
            ],
            toolbar: 'undo redo | styleselect | bold italic | bullist numlist | link image | alignleft aligncenter alignright alignjustify | outdent indent | code',
            toolbar_mode: 'sliding',
        });
    }

    // Init datatable
    if (document.querySelectorAll('#logs').length > 0) {
        const dataTable = new simpleDatatables.DataTable("#logs", {
            perPage: 50
        })
    }

    // Confirm action
    for (let element of document.querySelectorAll(".admin-delete")) {
        element.onclick = function () {
            let url = element.dataset.href;
            let text = element.dataset.popuptext;
            let title = element.dataset.popuptitle;
            that.confirm_popup(title, text, url);
        };
    }

    // Init word war
    if (document.querySelectorAll('#account-name').length > 0) {
        let accountinput = document.querySelector('#account-name');
        accountinput.addEventListener('change', (event) => {
            let creator = document.querySelector('#account-name').value;
            document.cookie = "wwaccount=" + creator;
            for (let input of document.querySelectorAll(".wordwars-container [name=\"creator\"]")) {
                input.value = creator;
            }
        });
    }

    // Init ww countdowns
    if (document.querySelectorAll("#ww-info").length > 0) {
        document.querySelector("#main-content").addEventListener('click', joinWw);

        function joinWw(e) {
            document.querySelector("#ww-info").style.display = "none";
            play_song('assets/audio/vide.mp3');
            if (document.querySelectorAll("#ww-begin-countdown").length > 0) {
                document.querySelector('#ww-begin-countdown').style.display = "block";
                that.init_begin_ww_countdown('ww-begin-countdown')
            }
            document.querySelector("#main-content").removeEventListener('click', joinWw);
        }
    }

    // Init word wars ambiance
    if (document.querySelectorAll("#ambiant").length > 0) {
        document.querySelector("#launch-ronron").onclick = function () {
            document.querySelector("#audio-ronron").style.visibility = "visible";
            document.querySelector("#audio-ronron").style.height = "50px";
            document.querySelector("#audio-ronron").play();
        };
    }
}

function init_delete_parent_comment() {
    for (let element of document.querySelectorAll(".remove-comment-parent")) {
        element.onclick = function () {
            document.querySelector('#add-comment-container .parent-comment-info').innerHTML = "";
            document.querySelector('#add-comment-parentid').value = 0;
        };
    }
}

function confirm_popup(title, text, url) {
    let output = '' +
        '<div id="popup">' +
        '<div class="overlay"></div>' +
        '<div class="popup-content">' +
        '<div class="header">' + title + '<div class="close">X</div></div>' +
        '<div class="popup-main-content">' +
        '<div class="message">' + text + '</div>' +
        '<div class="buttons">' +
        '<button class="confirm button">Confirmer</button>' +
        '<button class="close button button-inverse">Annuler</button>' +
        '</div>' +
        '</div>' +
        '</div> ' +
        '</div>';

    let popup = document.querySelector('#popup');
    if (popup) {
        popup.remove();
    }

    let body = document.querySelector('body');
    body.insertAdjacentHTML('afterend', output);

    for (let element of document.querySelectorAll("#popup .close")) {
        element.onclick = function () {
            let popup = document.querySelector('#popup');
            if (popup) {
                popup.remove();
            }
        };
    }

    for (let element of document.querySelectorAll("#popup .confirm")) {
        element.onclick = function () {
            window.location = url;
        };
    }
}

function init_begin_ww_countdown(id) {

    // Les temps venus du php sont en UTC, il faut les convertir
    var timezone_offset_minutes = new Date().getTimezoneOffset();
    timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
    let timebegin = document.querySelector('#' + id).dataset.timebegin;
    timebegin = parseInt(timebegin) + (timezone_offset_minutes * 60);
    // On enlève les milliseconds
    let now = Math.trunc(new Date().getTime() / 1000) + (timezone_offset_minutes * 60);

    let timeleft = timebegin - now;
    if (timeleft < 0) {
        document.querySelector('#' + id).style.display = "none";
        init_ww_circular_countdown('ww-circular-countdown');
    } else {
        // Update the count down every 1 second
        let interval = setInterval(function () {
            let now = Math.trunc(new Date().getTime() / 1000) + (timezone_offset_minutes * 60);
            let timeleft = timebegin - now;
            console.log('timebeign ', timebegin, 'now', now, 'timeleft', timeleft);
            if (timeleft < 0) {
                clearInterval(interval);
                document.querySelector('#' + id).style.display = "none";
                play_song('assets/audio/ww-begin.wav')
                init_ww_circular_countdown('ww-circular-countdown');
            }

            // Time calculations for minutes and seconds
            timeleft = timeleft;
            let hour = Math.trunc((timeleft / 3600));
            let min = Math.trunc((timeleft % 3600) / 60);
            let sec = Math.trunc(timeleft % 60);
            if (hour < 10) {
                hour = '0' + hour;
            }
            if (min < 10) {
                min = '0' + min;
            }
            if (sec < 10) {
                sec = '0' + sec;
            }

            if (hour > 0) {
                document.querySelector('#' + id + ' .countdown').innerHTML = hour + ':' + min + ':' + sec;
            } else {
                document.querySelector('#' + id + ' .countdown').innerHTML = min + ':' + sec;
            }


        }, 1000);
    }
}

function init_ww_circular_countdown(id) {
    let container = document.querySelector('#' + id);
    document.querySelector('#' + id).style.display = "block";
    let durationminutes = container.dataset.duration;
    let durationseconds = durationminutes * 60;

    // Les temps venus du php sont en UTC, il faut les convertir
    var timezone_offset_minutes = new Date().getTimezoneOffset();
    timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
    let timebegin = container.dataset.timebegin;
    timebegin = parseInt(timebegin) + (timezone_offset_minutes * 60);
    // On enlève les milliseconds
    let now = Math.trunc(new Date().getTime() / 1000) + (timezone_offset_minutes * 60);

    let timeend = (parseInt(timebegin) + parseInt(durationseconds));

    let timeleft = timeend - now;
    if (timeleft <= 0) {
        document.querySelector('#' + id).style.display = "none";
        document.querySelector('#ww-end').style.display = "block";
    } else {

        let pointer = document.querySelector('#' + id + ' #e-pointer');
        let progressBar = document.querySelector('#' + id + ' .e-c-progress');
        let length = Math.PI * 2 * 100;

        progressBar.style.strokeDasharray = length;

        let interval = setInterval(function () {
            let now = Math.trunc(new Date().getTime() / 1000) + (timezone_offset_minutes * 60);
            let timeleft = timeend - now;
            if (timeleft <= 0) {
                clearInterval(interval);
                play_song('assets/audio/ww-end.wav')
                document.querySelector('#' + id).style.display = "none";
                document.querySelector('#ww-end').style.display = "block";
                document.querySelector("#audio-ronron").pause();
            }
            let min = Math.trunc((timeleft % 3600) / 60);
            let sec = Math.trunc(timeleft % 60);
            if (min < 10) {
                min = '0' + min;
            }
            if (sec < 10) {
                sec = '0' + sec;
            }

            document.querySelector('#' + id + ' .display-remain-time').innerHTML = min + ':' + sec;

            update(timeleft, durationseconds);

        }, 1000);

        function update(value, timePercent) {
            var offset = -length - length * value / (timePercent);
            progressBar.style.strokeDashoffset = -offset;
            pointer.style.transform = `rotate(${-360 * value / (timePercent)}deg)`;
        };
    }
}

function play_song(name) {
    var audio = new Audio(name);
    audio.play();
}
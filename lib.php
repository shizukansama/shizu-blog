<?php

use PHPMailer\PHPMailer\PHPMailer;

require_once(__DIR__ . '/classes/database_interface.php');
require_once(__DIR__ . '/classes/page.class.php');
require_once(__DIR__ . '/classes/wordwar_session.class.php');

$dbinterface = new database_interface();

/* Utils */
function is_admin() {
    return isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == true;
}

function require_admin() {
    global $CFG;
    if (!is_admin()) {
        redirect($CFG->siteUrl . '/login.php');
    }
    return true;
}

function redirect($url) {
    header('Location: ' . $url);
}

function get_ip() {
    return $_SERVER['REMOTE_ADDR'];
}

function get_param($name, $type) {

    if (isset($_GET[$name])) {
        $value = $_GET[$name];
    } else if (isset($_POST[$name])) {
        $value = $_POST[$name];
    } else {
        return false;
    }
    switch ($type) {
        case "int":
            $value = (int) $value;
            break;
        case "string";
            $value = strip_tags($value);
            break;
    }

    return $value;
}

function login_admin($login, $password) {
    global $CFG;
    if ($login === $CFG->adminLogin && $password === $CFG->adminPassword) {
        $_SESSION['isAdmin'] = true;
        return true;
    } else {
        return false;
    }
}

function user_date($format, $date = '') {
    if($date == ''){
        $date = time();
    }
    $timezone = get_user_timezone();
    date_default_timezone_set($timezone);
    return date($format, $date);
}

function get_user_timezone(){
    return isset($_COOKIE["timezone"]) ? $_COOKIE["timezone"] : get_default_timezone();
}

function get_default_timezone_offset(){
    //$timezone = new DateTimeZone(get_default_timezone());

    //return timezone_offset_get(get_default_timezone());
}

function get_default_timezone() {
    return 'Europe/Paris';
    //return 'Europe/London';
}

function send_message($email, $subject, $message) {
    global $CFG;

    require 'assets/lib/PHPMailer/Exception.php';
    require 'assets/lib/PHPMailer/PHPMailer.php';
    require 'assets/lib/PHPMailer/SMTP.php';

    $mail = new  PHPMailer();

    $mail->IsSMTP();
    $mail->SMTPDebug = 0;

    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = "ssl";
    $mail->Host       = $CFG->emailHost;
    $mail->Port       = $CFG->emailPort;
    $mail->Username   = $CFG->emailUser;
    $mail->Password   = $CFG->emailPass;

    $mail->SetFrom($CFG->emailUser, $CFG->siteUrl);

    $mail->Subject = $subject;

    $mail->MsgHTML($message);

    $mail->AddAddress($email, "moi");

    if (!$mail->Send()) {
        return $mail->ErrorInfo;
    }

    return true;
}

/* Search */
function globalsearch_articles_results($search) {
    global $dbinterface;

    // Articles
    $matcharticles = $dbinterface->articles_search($search);
    foreach ($matcharticles as $key => $article) {
        if (!$article->is_visible()) {
            unset($matcharticles[$key]);
        }
    }

    return $matcharticles;

}

/* Article */
function get_frontpage_articles() {
    global $dbinterface;
    $isadmin          = is_admin();
    $allarticles      = $dbinterface->get_articles();
    $filteredarticles = [];
    foreach ($allarticles as $article) {
        if ($article->is_visible() || $isadmin) {
            $filteredarticles[] = $article;
        }
    }
    return $filteredarticles;
}

function get_more_articles($currentarticleid = 0, $max = 5) {
    global $dbinterface;
    $isadmin          = is_admin();
    $allarticles      = $dbinterface->get_articles();
    $filteredarticles = [];
    foreach ($allarticles as $article) {
        if ($article->get_id() == $currentarticleid) {
            continue;
        }
        if ($article->is_visible() || $isadmin) {
            $filteredarticles[] = $article;
        }

        if (count($filteredarticles) > $max) {
            break;
        }
    }
    return $filteredarticles;
}

function get_rss_articles() {
    global $dbinterface;
    $allarticles = $dbinterface->get_articles();
    $rssarticles = [];
    foreach ($allarticles as $article) {
        if ($article->is_visible()) {
            $rssarticles[$article->get_timestamp()] = $article;
        }
    }
    krsort($rssarticles);
    return $rssarticles;
}

function get_all_articles() {
    global $dbinterface;
    return $dbinterface->get_articles();
}

function get_article_by_id($articleid) {
    global $dbinterface;
    return $dbinterface->get_article_by_id($articleid);
}

function get_article_id_by_textid($textid) {
    global $dbinterface;
    return $dbinterface->get_article_id_by_textid($textid);
}

function get_article_by_textid($textid) {
    global $dbinterface;
    $article = $dbinterface->get_article_by_text_id($textid);
    if (!$article) {
        return false;
    }
    $comments = get_article_comments($article->get_id());
    $article->set_comments($comments);
    return $article;
}

function is_article_textid($textid) {
    global $dbinterface;
    return $dbinterface->is_article_textid($textid);
}

function add_article($textid, $title, $content, $description, $visibility, $date) {
    global $dbinterface;
    $dbinterface->add_article($textid, $title, $content, $description, $visibility, $date);
}

function edit_article($articleid, $updateddata) {
    global $dbinterface;
    $dbinterface->edit_article($articleid, $updateddata);
}

function delete_article($articleid) {
    global $dbinterface;
    $dbinterface->delete_article($articleid);
}

function add_article_comment($articleid, $parentcommentid, $username, $content, $email = '') {
    global $dbinterface;
    return $dbinterface->add_article_comment($articleid, $parentcommentid, $username, $content, $email);
}

function edit_article_comment($commentid, $updateddata) {
    global $dbinterface;
    $dbinterface->edit_article_comment($commentid, $updateddata);
}

function delete_article_comment($commentid) {
    global $dbinterface;
    $dbinterface->delete_article_comment($commentid);
}

/* Comment */
function get_article_comments($articleid) {
    global $dbinterface;
    $isadmin          = is_admin();
    $allcomments      = $dbinterface->get_article_comments($articleid);
    $filteredcomments = [];
    foreach ($allcomments as $comment) {
        // TODO, on doit pouvoir voir ses propres commentaires non validé par adresse IP
        if ($comment->is_visible() || $isadmin) {
            $filteredcomments[$comment->get_id()] = $comment;
        }
    }

    return $filteredcomments;
}

function get_all_article_comments($articleid) {
    global $dbinterface;
    return $dbinterface->get_article_comments($articleid);
}

function get_not_valided_comments_by_articles() {
    global $dbinterface;
    $comments          = $dbinterface->get_unvalidated_comments();
    $commentsbyarticle = [];
    $articlesMap       = [];
    foreach ($comments as $comment) {
        $articleid = $comment->get_article_id();
        if (!array_key_exists($articleid, $articlesMap)) {
            $article = get_article_by_id($articleid);
            if (!$article) {
                continue;
            }
            $articlesMap[$articleid]                   = $article;
            $commentsbyarticle[$article->get_textid()] = [];
        }
        $article                                     = $articlesMap[$articleid];
        $commentsbyarticle[$article->get_textid()][] = $comment;
    }
    return $commentsbyarticle;
}

/* Page */
function get_all_pages() {
    global $dbinterface;
    return $dbinterface->get_pages();
}

function is_page_textid($textid) {
    global $dbinterface;
    return $dbinterface->is_page_textid($textid);
}

function get_page_by_textid($textid) {
    global $dbinterface;
    $page = $dbinterface->get_page_by_textid($textid);
    if (!$page) {
        return false;
    }
    return $page;
}

function get_page_by_id($id) {
    global $dbinterface;
    return $dbinterface->get_page_by_id($id);
}

function add_page($textid, $title, $content, $visibility) {
    global $dbinterface;
    return $dbinterface->add_page($textid, $title, $content, $visibility);
}

function edit_page($pageid, $updateddata) {
    global $dbinterface;
    return $dbinterface->edit_page($pageid, $updateddata);
}

function delete_page($pageid) {
    global $dbinterface;
    $dbinterface->delete_page($pageid);
}

/* Newsletter */
function subscribe_newsletter($email) {
    global $dbinterface;
    $dbinterface->add_email_to_newsletter($email);
}

function unsubscribe_newsletter($email) {
    global $dbinterface;
    $dbinterface->remove_email_to_newsletter($email);
}

/* Word wars */
function get_wordwar_session($id) {
    global $dbinterface;
    $record = $dbinterface->get_wordwar_session($id);
    if (!$record) {
        return false;
    }
    return new wordwar_session($record);
}

function add_wordwar_session($creator, $timebegin, $duration) {
    global $dbinterface;

    $creator   = $creator == '' ? get_default_name() : $creator;
    $ip        = get_ip();
    return $dbinterface->add_wordwar_session($creator, $ip, $timebegin, $duration);
}

function get_open_wordwar_sessions() {
    global $dbinterface;
    return $dbinterface->get_open_wordwar_sessions();
}

function get_soon_wordwar_sessions($mintime) {
    global $dbinterface;
    return $dbinterface->get_wordwar_sessions($mintime);
}

function get_wordwar_participants($wwid) {
    global $dbinterface;
    $records = $dbinterface->get_wordwar_participants($wwid);

    $participants = [];
    foreach ($records as $record) {
        $participants[$record['ip']] = $record['name'];
    }

    return $participants;
}

function add_wordwar_participant($wwid, $ip, $name) {
    global $dbinterface;

    // S'il y a un particpant avec le bon nom et la bonne ip, on sort
    if ($dbinterface->get_wordwar_participant($wwid, $ip, $name)) {
        return true;
    }

    // S'il n'y a aucun participant avec cette ip, on l'ajoute
    $participant = $dbinterface->get_wordwar_participant_from_ip($wwid, $ip);
    if ($participant && $participant->name != $name && $participant->name == get_default_name()) {
        return $dbinterface->update_wordwar_participant($participant->id, $name);
    }

    // S'il y a un participant avec cette ip, s'il s'appelle Anonyme, on update, sinon on ajoute
    return $dbinterface->add_wordwar_participant($wwid, $name, $ip);
}

function get_ww_name() {
    return isset($_COOKIE["wwaccount"]) ? $_COOKIE["wwaccount"] : get_default_name();
}

function get_default_name() {
    return 'Anonyme';
}

function get_wordwar_next_time($startime, $minuteshift = 0) {
    // Gestion du cas où l'on arrive très proche de la prochaine minute
    $second = date('s', $startime);
    if ($second >= 58) {
        $startime += 60 - $second;
        $second   = 0;
    }
    $minute = date('i', $startime) % 10;

    if ($minute < 5) {
        // On va aller au prochaine *5min
        $rounddate = $startime + 60 * (5 - $minute) - $second;

    } else {
        // On va aller au prochaine *0min
        $rounddate = $startime + 60 * (10 - $minute) - $second;
    }

    return $rounddate + ($minuteshift * 60);
}

/* Logs */
function add_log($page = '', $type = '', $eventname = '', $data = null) {
    global $dbinterface;

    $ip        = get_ip();
    $page      = $page != '' ? $page : $_SERVER['REQUEST_URI'];
    $type      = $type != '' ? $type : 'view';
    $eventname = $eventname != '' ? $eventname : 'view';
    $data      = $data != null ? $data : array_merge($_POST, $_GET);
    $timestamp = time();

    return $dbinterface->add_log($ip, $page, $type, $eventname, $data, $timestamp);
}

function get_logs() {
    global $dbinterface;

    return $dbinterface->get_logs();
}
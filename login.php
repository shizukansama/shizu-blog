<?php

require_once 'config.php';

$login    = get_param('login', 'string');
$password = get_param('password', 'string');

if ($login && $password) {
    if (login_admin($login, $password)) {
        add_log('login.php', 'login', 'login\login', ['login' => $login]);
        redirect($CFG->siteUrl . '/admin.php');
        die();
    } else {
        $errormessage = "Identifiants incorrects";
        add_log('login.php', 'error', 'login\error', [
            'login'    => $login,
            'password' => $password
        ]);
    }
} else {
    add_log('login.php', 'view', 'login\view', []);
}

$title = "Login";

require('templates/login.php');
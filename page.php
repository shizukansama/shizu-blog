<?php

require_once 'config.php';

$textid = get_param('id', 'string');
$page = null;
if($textid){
    $page = get_page_by_textid($textid);
}
if (!$page ||(!is_admin() && !$page->is_visible())) {
    $hasothercontent = true;
    $articles = get_more_articles();
    $articlelisttitle = 'Accès aux articles qui existent !';
    $templateother = 'articles_list.php';

    $title = 'Perdu ?';

    $content = '<div class="error-page"><h2>Perdu ?</h2>
            <p>La page que vous recherchez n\'existe pas.</p>
            <p>Mais voici des articles qui pourraient vous intéresser !</p></div>';
    require('templates/base.php');
    die;
}

add_log('page.php', 'view', 'page\view', [
    'pageid' => $page->get_id()
]);

$menuselected = $page->get_textid();
$title           = $page->get_title();
$hasothercontent = true;

$articles = get_more_articles();
$articlelisttitle = 'Découvrir';
$templateother = 'articles_list.php';

$content = '<div class="standard-page-container"><h2>'.$page->get_title().'</h2><div class="page-content">'.$page->get_content() . '</div></div>';

$template = 'base.php';

require('templates/base.php');
<?php
    header ( "Content-type: text/xml" ) ;
    require_once 'config.php';
    $articles = get_rss_articles();
    $lastbuilddate = array_key_first($articles);

echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<rss version="2.0">
    <channel>
        <title>Derrière les pages</title>
        <lastBuildDate><?= date(DATE_RSS, $lastbuilddate) ?></lastBuildDate>
        <link><?= $CFG->siteUrl ?></link>
        <language>fr</language>
        <?php foreach($articles as $article) { ?>
        <item>
            <title><?= $article->get_title() ?></title>
            <link><?= $article->get_url() ?></link>
            <description><?= $article->get_content() ?></description>
            <pubDate><?= date(DATE_RSS, $article->get_timestamp()) ?></pubDate>
            <creator>Shizukan</creator>
            <comments>
                <?= $article->get_url() . '#add-comment-container' ?>
            </comments>
        </item>
        <?php } ?>
    </channel>
</rss>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= $title ?></title>
    <?php require_once('templates/head.php'); ?>

</head>

<body>

<header id="main-header">
    Derrière "Derrière les pages"
</header>

<div id="header-menu" class="horizontal-menu">
    <div class="menu-element <?php if(isset($menuselected) && 'frontpage' == $menuselected) echo 'selected' ?>"><a href="./admin.php">Accueil</a></div>
    <div class="menu-element <?php if(isset($menuselected) && 'managearticles' == $menuselected) echo 'selected' ?>"><a href="./admin.php?page=articles">Gestion des articles</a></div>
    <div class="menu-element <?php if(isset($menuselected) && 'managecomments' == $menuselected) echo 'selected' ?>"><a href="./admin.php?page=comments">Gestion des commentaires</a></div>
    <div class="menu-element <?php if(isset($menuselected) && 'managepages' == $menuselected) echo 'selected' ?>"><a href="./admin.php?page=pages">Gestion des pages</a></div>
    <div class="menu-element <?php if(isset($menuselected) && 'logs' == $menuselected) echo 'selected' ?>"><a href="./admin.php?page=logs">Logs</a></div>
    <div class="menu-element"><a href="./">Retour au site</a></div>
</div>

<div id="page-content">
    <div id="admin-main-content">
        <?php if(isset($template)){
            require_once ('templates/'. $template);
        } ?>
    </div>

</div>

<footer>
    <a href="#">À propos</a>
    <a href="#">Contact</a>
</footer>

</body>
</html>
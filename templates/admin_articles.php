<div class="admin-articles-container">

    <?php foreach($articles as $article){ ?>
        <div class="article-container">
            <h3 class="inline-title"><?php if(!$article->is_visible()){ ?><span>(Caché)</span> <?php } ?><?= $article->get_title() ?></h3>

            <div class="admin-action">
                <?php if($article->is_visible()){ ?>
                    <a href="admin.php?page=articles&action=hide&articleid=<?= $article->get_id() ?>">Cacher</a>
                <?php }else{ ?>
                    <a href="admin.php?page=articles&action=show&articleid=<?= $article->get_id() ?>">Afficher</a>
                <?php } ?>

                <a href="admin.php?page=edit-article&articleid=<?= $article->get_id() ?>"><img class="icon" src="<?= $CFG->siteUrl ?>/assets/img/icon-edit.svg" title="Éditer"></a>
                <span class="admin-delete" title="Supprimer" data-popuptitle="Suppression d'article" data-popuptext="Confirmez-vous la suppression de l'article : <?= $article->get_title() ?> ?"
                      data-href="admin.php?page=articles&action=delete&articleid=<?= $article->get_id() ?>"><img class="icon" title="Supprimer" src="<?= $CFG->siteUrl ?>/assets/img/icon-delete.svg"></span>
            </div>
            <div><?= $article->get_description() ?></div>
            <div><?= user_date('d/m/Y à H:i', $article->get_timestamp()) ?></div>
        </div>
    <?php } ?>

    <a href="admin.php?page=add-article"><button class="button">Ajouter un article</button></a>
</div>
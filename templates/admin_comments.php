<div class="admin-comments-container">
    <h2><?= $nbComments ?> commentaire non validés</h2>
    <?php foreach($commentsbyarticles as $articletextid => $comments){ ?>
        <div class="admin-article-comments">
            <h3>Article : <?= $articletextid ?></h3>
            <?php foreach($comments as $comment){ ?>
                <div class="admin-comment-container">
                    <?= $comment->get_date() ?> - <?= $comment->get_username() ?>

                    <span class="action">
                        <?php if($comment->is_visible()){ ?>
                            <a href="admin.php?page=comments&action=hide&commentid=<?= $comment->get_id() ?>">Cacher</a>
                        <?php }else{ ?>
                            <a href="admin.php?page=comments&action=show&commentid=<?= $comment->get_id() ?>">Valider</a>
                        <?php } ?>
                        <span class="admin-delete" title="Supprimer" data-popuptitle="Suppression d'un commentaire" data-popuptext="Confirmez-vous la suppression du commentaire de  : <?= $comment->get_username() ?> ?"
                               data-href="admin.php?page=comments&action=delete&commentid=<?= $comment->get_id() ?>">X</span>
                    </span>
                    <div>
                        <p>Email : <?= $comment->get_email() != '' ? $comment->get_email() : "Non renseigné" ?></p>
                        <p>IP : <?= $comment->get_ip() ?></p>
                        <?= $comment->get_content() ?>
                    </div>
                </div>
            <?php } ?>
        </div>

    <?php } ?>
</div>
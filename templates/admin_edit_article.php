<form id="add-article" method="POST" action="">

    <input id="articleid" name="articleid" type="hidden" value="<?php if($article) echo $article->get_id() ?>">

    <div class="form-element">
        <label for="title">Titre <span class="required">*</span></label>
        <input id="title" name="title" type="text" value="<?php if($articletitle){ echo htmlspecialchars($articletitle);} else if($article){ echo htmlspecialchars($article->get_title());} ?>" required>
    </div>

    <div class="form-element">
        <label>Identifiant textuel  <span class="required">*</span></label>
        <input id="textid" name="textid" type="text" value="<?php if($articletextid){ echo $articletextid;} else if($article){ echo $article->get_textid();} ?>" required><span class="input-info"> (ne doit pas contenir d'espace et autres caractères invalides pour un lien)</span>
    </div>

    <div class="form-element">
        <label for="description">Description</label>
        <textarea name="description" id="description" class="rich-editor"><?php if($articledescription){ echo $articledescription;} else if($article){ echo $article->get_description();} ?></textarea>
    </div>

    <div class="form-element">
        <label for="content">Contenu</label>
        <textarea name="content" id="content" class="rich-editor"><?php if($articlecontent){ echo $articlecontent;} else if($article){ echo $article->get_content();} ?></textarea>
    </div>

    <div class="form-element">
        <label for="date">Date</label>
        <input type="datetime-local" id="date" name="date" value="<?php
        if(!$article){
            echo date('Y-m-d') . 'T' . date('H:i');
        }else if($articledate){
            echo $articledate;
        }else{
           echo date('Y-m-d', $article->get_timestamp()) . 'T' . date('H:i', $article->get_timestamp()) ;
        } ?>">
    </div>

    <input type="submit" class="button" value="Ajouter">
</form>
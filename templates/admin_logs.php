<div class="admin-logs-container">

    <table id="logs">
        <thead>
            <tr><th>IP</th><th>Date (Y-m-d)</th><th>Page</th><th>Type</th><th>Data</th></tr>
        </thead>
        <?php foreach ($logs as $log){ ?>
            <tr>
                <td><?= $log['ip'] ?></td>
                <td><?= date('Y-m-d à H:i',$log['timecreated']) ?></td>
                <td><?= $log['page'] ?></td>
                <td><?= $log['type'] ?></td>
                <td><?= $log['data'] ?></td>
            </tr>
        <?php } ?>
    </table>

</div>
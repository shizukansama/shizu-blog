<form id="add-page" method="POST" action="">

    <input id="pageid" name="pageid" type="hidden" value="<?php if($page){ echo $page->get_id(); }?>">
    <div class="form-element">
        <label for="title">Titre <span class="required">*</span></label>
        <input id="title" name="title" type="text" value="<?php if($pagetitle){ echo $pagetitle;} else if($page){echo $page->get_title(); }?>" required>
    </div>

    <div class="form-element">
        <label>Identifiant textuel  <span class="required">*</span></label>
        <input id="textid" name="textid" type="text" value="<?php if($pagetextid){ echo $pagetextid;} else if($page) echo $page->get_textid() ?>" required><span class="input-info"> (ne doit pas contenir d'espace et autres caractères invalides pour un lien)</span>
    </div>

    <div class="form-element">
        <label for="content">Contenu</label>
        <textarea name="content" id="content" class="rich-editor"><?php if($pagecontent){ echo $pagecontent;} else if($page) echo $page->get_content() ?></textarea>

    </div>

    <input type="submit" class="button" value="Ajouter">
</form>
<div class="admin-pages-container">

    <?php foreach($pages as $page){ ?>
        <div class="page-container">
            <h3 class="inline-title"><?php if(!$page->is_visible()){ ?><span>(Caché)</span> <?php } ?><?= $page->get_title() ?></h3>

            <div class="admin-action">
                <?php if($page->is_visible()){ ?>
                    <a href="admin.php?page=pages&action=hide&pageid=<?= $page->get_id() ?>">Cacher</a>
                <?php }else{ ?>
                    <a href="admin.php?page=pages&action=show&pageid=<?= $page->get_id() ?>">Afficher</a>
                <?php } ?>

                <a href="admin.php?page=edit-page&pageid=<?= $page->get_id() ?>">Editer</a>
                <span class="admin-delete" title="Supprimer" data-popuptitle="Suppression d'une page" data-popuptext="Confirmez-vous la suppression de la page : <?= $page->get_title() ?> ?"
                      data-href="admin.php?page=pages&action=delete&pageid=<?= $page->get_id() ?>">X</span>
            </div>
        </div>
    <?php } ?>


    <a href="admin.php?page=add-page"><button class="button">Ajouter une page</button></a>
</div>
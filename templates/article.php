<div class="article-page">
    <div class="article-content-container">
        <h2 class="article-title"><?= $article->get_title() ?>
        <?php if(is_admin()){ ?>
        <span class="admin-action">
            <span><?php if(!$article->is_visible()){ ?><span>(Caché)</span> <?php } ?>
            <a href="<?= $CFG->siteUrl . '/admin.php?page=edit-article&articleid=' . $article->get_id() ?>">Editer</a>
        </span>
        <?php } ?>
        </h2>
        <div class="article-content">
            <?= $article->get_content() ?>
        </div>
        <div class="article-info"><?= "Écrit par Shizu le ". user_date('d/m/Y à H:i', $article->get_timestamp()) ?></div>
    </div>
    <div class="comments-container">
        <h2>Vos participations :</h2>
        <?php  if(!$article->get_comments()){ ?>
                Aucun commentaire
        <?php  }else{
            foreach($article->get_comments() as $comment){
                $offset = 0;
                include 'comment.php';
            }   ?>
        <?php } ?>

        <div id="add-comment-container">
            <h3>Participer :</h3>

            <p class="form-info">Votre adresse email ne sera pas publiée. Les champs obligatoires sont indiqués avec *</p>
            <form method="POST" action="">

                <input type="hidden" name="action" value="addcomment">

                <div class="parent-comment-info"></div>
                <input type="hidden" id="add-comment-parentid" name="add-comment-parentid" value="<?php if(isset($parentcommentid)){ echo $parentcommentid; }else{ echo 0; } ?>">

                <div class="form-element">
                    <label for="add-comment-username">Votre nom <span class="required">*</span></label>
                    <input type="text" name="add-comment-username" id="add-comment-username" value="<?php if(isset($commentusername)) echo $commentusername; ?>" required>
                </div>

                <div class="form-element">
                    <label for="comment-content">Commentaire <span class="required">*</span></label>
                    <textarea id="add-comment-content" name="add-comment-content" required><?php if(isset($commentcontent)) echo $commentcontent; ?></textarea>
                </div>

                <div class="form-element">
                    <label for="add-comment-email">Email</label>
                    <input type="email" name="add-comment-email" id="add-comment-email" value="<?php if(isset($commentemail)) echo $commentemail; ?>" >
                    <span class="input-info">Si vous souhaitez que je puisse vous contacter</span>
                </div>

                <input type="submit" class="button" value="Envoyer le commentaire">
            </form>

        </div>
    </div>
</div>
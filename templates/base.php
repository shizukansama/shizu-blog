<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
    <?php require_once('templates/head.php'); ?>
</head>

<body>

<header id="main-header">
    <a class="sitename" href="<?= $CFG->siteUrl ?>">Derrière les pages</a>
    <div class="header-actions">
        <a href="." class="header-element <?php if( isset($menuselected) && 'frontpage' == $menuselected) echo 'selected' ?>">Accueil</a>
        <a href="wordwars.php" class="header-elementt">Word Wars</a>
        <a href="contact.php" class="header-element <?php if(isset($menuselected) && 'contact' == $menuselected) echo 'selected' ?>">Contact</a>
        <a href="a-propos" class="header-element <?php if(isset($menuselected) && 'a-propos' == $menuselected) echo 'selected' ?>">À propos</a>
        <?php if(is_admin()){ ?>
            <a href="admin.php" class="header-element <?php if(isset($menuselected) && 'admin' == $menuselected) echo 'selected' ?>">Administration</a>
        <?php } ?>
    </div>
</header>

<div id="page-content">

    <div id="main-content">
        <?php if(isset($errormessage)){ ?><div class="alert alert-error"><?= $errormessage ?></div><?php  } ?>
        <?php if(isset($successmessage)){ ?><div class="alert alert-success"><?= $successmessage ?></div><?php  } ?>

        <?php if(isset($content)) echo $content; ?>
        <?php if(isset($template)){
            require_once ('templates/'. $template);
        } ?>
    </div>

    <?php if(isset($hasothercontent) && $hasothercontent){ ?>
    <div id="other-content">
        <?php if(isset($othercontent)) echo $othercontent ?>
        <?php if(isset($templateother)) {
            require_once ('templates/'. $templateother);
        } ?>
        <?php include ('templates/socials.php');?>

    </div>
    <?php } ?>
</div>

<footer>
    <a href="a-propos">À propos</a>
    <a href="contact.php">Contact</a>
    <a href="mentions-legales">Mentions légales</a>
</footer>

</body>
</html>
<div class="comment-container offset-<?= $offset ?>" data-commentid="<?= $comment->get_id() ?>">
    <div class="comment-user">
        <div class="comment-name"><?= $comment->get_username() ?></div>
        <div class="comment-date"><?= $comment->get_date() ?></div>
        <?php if(is_admin() && !$comment->is_validated()){ ?>
            <div class="comment-status">En attente de validation</div>
        <?php } ?>
    </div>
    <div class="comment-content-container">
        <div class="comment-content"><?= $comment->get_content() ?></div>
        <div class="comment-actions">
            <span class="comment-response" data-commentid="<?= $comment->get_id() ?>">Répondre</span>
        </div>
    </div>
</div>
<?php if($comment->get_children()) {
    $children = $comment->get_children();
    $tempoffset = $offset;
    $offset ++;
    foreach ($children as $comment){
        include 'comment.php';
        $offset = $tempoffset;
    }

} ?>
<div id="contact-page-container">
<h2>Me contacter</h2>

<p>Vous avez des questions, des remarques, des suggestions de tout type, n'hésitez pas à me contacter ici ou réagir directement sur les articles :)</p>

<p>Vous pouvez aussi me contacter directement sur le serveur <b>Discord</b> du blog en <a href="<?= $CFG->discordUrl ?>">cliquant ici</a> !</p>

<form method="POST" action="">

    <div class="form-element">
        <label for="add-comment-username">Votre nom <span class="required">*</span></label>
        <input type="text" name="contact-username" id="contact-username" value="<?= $contactusername ?>" required>
    </div>

    <div class="form-element">
        <label for="contact-email">Votre email <span class="required">*</span></label>
        <input type="email" name="contact-email" id="contact-email" value="<?= $contactemail ?>" required></div>

    <div class="form-element">
        <label for="contact-message">Votre message <span class="required">*</span></label>
        <textarea id="contact-message" name="contact-message" required><?= $contactmessage ?></textarea>
    </div>

<!--  TODO  Ajouter un captcha-->

    <input type="submit" class="button" value="Envoyer">

</form>
</div>
<div id="frontpage-list">
    <?php foreach ($articles as $article) { ?>
        <div class="article-block <?php if(!$article->is_visible()) echo 'not-visible'; ?>">
            <h2 class="article-title"><img class="icon" src="<?= $CFG->siteUrl ?>/assets/img/mug-blue.png"><a href="<?= $article->get_article_link()?>"> <?= $article->get_title() ?></a> <?php if(!$article->is_visible()) echo ' (caché)'; ?></h2>
            <div class="article-date">Publié le <?= user_date('d/m/Y à H:i', $article->get_timestamp()) ?></div>
            <div class="article-description"><?= $article->get_description() ?></div>
            <div class="article-right-link">
                <a href="<?= $article->get_article_link() ?>">Lire la suite</a>
            </div>
            </div>
    <?php } ?>
</div>
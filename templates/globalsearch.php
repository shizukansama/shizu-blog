<div id="global-search-container">

    <div class="search">
        <h2>Rechercher</h2>
        <form action="<?= $CFG->siteUrl ?>/globalsearch.php" method="post">
            <div class="form-element">
                <input type="text" name="search" placeholder="Votre recherche" minlength="3" value="<?= $search ?>" required>
                <input type="submit" class="button-img search" value="Rechercher" style="background: url('<?= $CFG->siteUrl ?>/assets/img/icon-search.svg') no-repeat center top;">
            </div>
        </form>
    </div>

    <div class="search-results">
        <h2>Résultats de la recheche</h2>

        <?php if(!$matcharticles){ ?>
            <p>Aucun résultat</p>
        <?php }else {
            foreach ($matcharticles as $article){ ?>
                <div class="article-search">
                    <h3><a href="<?= $article->get_article_link() ?>"> > <?= $article->get_title() ?></a></h3>
                </div>
            <?php }
        } ?>

    </div>

</div>
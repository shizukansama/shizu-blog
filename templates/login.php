<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title><?= $title ?></title>
    <script src="js/common.js"></script>
    <link href="styles.css" rel="stylesheet"/>
</head>

<body>

<header id="main-header">
    Derrière "Derrière les pages"
</header>

<div id="page-content">
    <div id="login-content">
        <?php if(isset($errormessage)){ ?><div class="alert alert-error"><?= $errormessage ?></div><?php  } ?>
        <form method="POST" action="">

            <div class="form-element">
                <label for="add-comment-username">Nom d'utilisateur <span class="required">*</span></label>
                <input type="text" name="login" id="login" required>
            </div>
            <div class="form-element">
                <label for="add-comment-username">Mot de passe <span class="required">*</span></label>
                <input type="password" name="password" id="password" required>

            </div>
            <input type="submit" class="button" value="Connexion">
        </form>
    </div>
</div>

<footer>
    <a href=".">Retour à l'accueil</a>
</footer>

</body>
</html>
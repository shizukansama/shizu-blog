<div class="search-container">
    <h3 class="title">Rechercher</h3>
    <form action="<?= $CFG->siteUrl ?>/globalsearch.php" method="post">
        <div class="form-element">
            <input type="text" name="search" placeholder="Votre recherche" minlength="3" required>
            <input type="submit" class="button-img search" value="Rechercher" style="background: url('<?= $CFG->siteUrl ?>/assets/img/icon-search.svg') no-repeat center top;">
        </div>
    </form>
</div>

<div class="socials">
    <div class="social rss">
        <a href="<?= $CFG->siteUrl . '/rss.xml' ?>" target="_blank" title="Flux RSS"><img src="<?= $CFG->siteUrl ?>/assets/img/icon-rss.svg"></a>
    </div>
    <div class="social twitter">
        <a href="https://twitter.com/derriere_pages" target="_blank" title="Twitter"><img src="<?= $CFG->siteUrl ?>/assets/img/icon-twitter.svg"></a>
    </div>
    <div class="social discord">
        <a href="<?= $CFG->discordUrl ?>" target="_blank" title="Discord"><img src="<?= $CFG->siteUrl ?>/assets/img/icon-discord.svg"></a>
    </div>
</div>

<!--<div class="newsletter">
    <h3>Inscription à la newsletter</h3>
    <p>Envie de recevoir des nouvelles à la sortie d'un nouvel article et peut-être plus&#160;?</p>
    <form action="" method="post">
        <input type="text" placeholder="Votre adresse email">
        <div><input type="submit" class="button button-inverse" value="S'inscrire"></div>
    </form>
</div>-->
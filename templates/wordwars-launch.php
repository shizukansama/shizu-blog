<div class="wordwars-container launch-wordwar">

    <div class="top-action">
        <div id="ww-account" class="account">
            <label>Votre nom (facultatif)</label>
            <input type="text" id="account-name" name="account-name" placeholder="Anonyme" value="<?= $participantname ?>">
        </div>
        <div id="ambiant">
            <div class="ambiant">
                <button id="launch-ronron" class="button button-inverse">Lancer des ronrons</button>
                <br>
                <audio id="audio-ronron" controls loop>
                    <source src="assets/audio/ronrons.mp3" type="audio/mp3">
                </audio>
            </div>

        </div>
    </div>

    <div class="center-content">
        <?php if (!$isdone) { ?>
            <div id="ww-help">
                <p>Partagez ce lien (ou celui de la page) aux autres participants !</p>
                <p class="ww-link"><?= $wwsession->get_url() ?></p>
            </div>
            <div id="ww-info">
                <button class="button big-button">Rejoindre la word war</button>
            </div>

            <div id="ww-begin-countdown" class="countdown-container" data-timebegin="<?= $wwsession->get_timebegin_timestamp() ?>">
                <h2>Commence dans </h2>
                <div class="countdown">..:..:..</div>
            </div>

            <div id="ww-circular-countdown" class="circular-countdown-container" data-duration="<?= $wwsession->get_duration() ?>" data-timebegin="<?= $wwsession->get_timebegin_timestamp() ?>">
                <h2>C'est parti !</h2>
                <div class="circle">
                    <svg width="300" viewBox="0 0 220 220" xmlns="http://www.w3.org/2000/svg">
                        <g transform="translate(110,110)">
                            <circle r="100" class="e-c-base"/>
                            <g transform="rotate(-90)">
                                <circle r="100" class="e-c-progress"/>
                                <g id="e-pointer">
                                    <circle cx="100" cy="0" r="8" class="e-c-pointer"/>
                                </g>
                            </g>
                        </g>
                    </svg>

                </div>
                <div class="controlls">
                    <div class="display-remain-time"><?= $wwsession->get_duration() ?>:00</div>
                </div>
            </div>

        <?php } ?>

        <div id="ww-end" class="<?php if ($isdone) {
            echo 'displayed';
        } ?>">
            <h2>Word war terminée !</h2>
        </div>
    </div>


    <div class="bottom-action">
        <div id="ww-participants">
            <h3>Participants</h3>
            <ul>
                <?php foreach ($wwparticpants as $participant) { ?>
                    <li><?= $participant ?></li>
                <?php } ?>
            </ul>
        </div>

        <a class="buttons" href="wordwars.php">
            <button class="button button-inverse button-ww-return">Retour aux word wars</button>
        </a>
    </div>

</div>
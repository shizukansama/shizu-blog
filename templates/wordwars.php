<div class="select-wordwars-container">

    <div id="ww-account" class="account">
        <label>Votre nom (facultatif)</label>
        <input type="text" id="account-name" name="account-name" placeholder="Anonyme" value="<?= $participantname ?>">
    </div>

    <div class="create-wordwars">
        <h2>Lancer une word war</h2>
        <div class="shortcuts">
            <form class="shortcut" method="post">
                <input type="hidden" name="action" value="create-wordwar">
                <input type="hidden" name="creator" value="<?= $participantname ?>">
                <input type="hidden" name="starttime" value="<?= $nextww ?>">
                <input type="hidden" name="duration" value="15">
                <input type="submit" class="button" value="Lancer à <?= user_date('H:i', $nextww) ?> pour 15min">
            </form>

            <form class="shortcut" method="post">
                <input type="hidden" name="action" value="create-wordwar">
                <input type="hidden" name="creator" value="<?= $participantname ?>">
                <input type="hidden" name="starttime" value="<?= $nextww ?>">
                <input type="hidden" name="duration" value="20">
                <input type="submit" class="button" value="Lancer à <?= user_date('H:i', $nextww) ?> pour 20min">
            </form>
            <br>
            <form class="shortcut" method="post">
                <input type="hidden" name="action" value="create-wordwar">
                <input type="hidden" name="creator" value="<?= $participantname ?>">
                <input type="hidden" name="starttime" value="<?= $secondnextww ?>">
                <input type="hidden" name="duration" value="15">
                <input type="submit" class="button" value="Lancer à <?= user_date('H:i', $secondnextww) ?> pour 15min">
            </form>

            <form class="shortcut" method="post">
                <input type="hidden" name="action" value="create-wordwar">
                <input type="hidden" name="creator" value="<?= $participantname ?>">
                <input type="hidden" name="starttime" value="<?= $secondnextww ?>">
                <input type="hidden" name="duration" value="20">
                <input type="submit" class="button" value="Lancer à <?= user_date('H:i', $secondnextww) ?> pour 20min">
            </form>
        </div>

        <form method="post" class="add-wordwars-form">
            <input type="hidden" name="action" value="create-wordwar">
            <input type="hidden" name="creator" value="<?= $participantname ?>">
            <label><b>Date de début</b></label>
            <div class="form-date form-element">
                Le
                <select name="year">
                    <?php
                    $currentyear = user_date('Y', $nextww);
                    foreach ($years as $year) { ?>
                        <option value="<?= $year ?>" <?php if ($currentyear == $year) echo "selected" ?>><?= $year ?></option>
                    <?php } ?>
                </select>

                <select name="month">
                    <?php
                    $currentmonth = user_date('n', $nextww);
                    foreach ($months as $number => $month) { ?>
                        <option value="<?= $number ?>" <?php if ($currentmonth == $number) echo "selected" ?>><?= $month ?></option>
                    <?php } ?>
                </select>

                <select name="day">
                    <?php
                    $currentday = user_date('d', $nextww);
                    for ($i = 1; $i <= $days; $i++) { ?>
                        <option value="<?= $i ?>" <?php if ($currentday == $i) echo "selected" ?>><?= $i ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-element form-hour ">
                À
                <select name="hour">
                    <?php
                    $currenthour = user_date('H', $nextww);
                    for ($i = 0; $i <= 23; $i++) { ?>
                        <option value="<?= $i < 10 ? 0 . $i : $i ?>" <?php if ($currenthour == $i) echo "selected" ?>>
                            <?php echo $i < 10 ? 0 . $i : $i ?>
                        </option>
                    <?php } ?>
                </select>
                h
                <select name="minute">
                    <?php
                    $currentminute = user_date('i', $nextww);
                    for ($i = 0; $i <= 59; $i++) { ?>
                        <option value="<?= $i < 10 ? 0 . $i : $i ?>" <?php if ($currentminute == $i) echo "selected" ?>>
                            <?php echo $i < 10 ? 0 . $i : $i ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-element">
                <label><b>Pour combien de minutes ?</b></label>
                <input type="number" name="duration" size="3" min="1" max="240" value="20"> minutes
            </div>

            <div class="form-element">
                <input type="submit" class="button" value="C'est parti !">
            </div>
        </form>
    </div>
</div>

<div class="wordwars-list">
    <h3>Word wars en cours !</h3>
    <?php if (!$openwordwars) { ?>
        <p>Aucune wordwar en cours.</p>
    <?php } ?>
    <ul>
        <?php foreach ($openwordwars as $wordwar) { ?>
            <li><a href="<?= $wordwar->get_url() ?>">Jusqu'au <b><?= user_date('d/m/Y à H:i',$wordwar->get_timeend_timestamp()) ?></b> (par <?= $wordwar->get_creator() ?>)</a></li>
        <?php } ?>
    </ul>

    <h3>Word wars bientôt disponibles</h3>
    <?php if (!$soonwordwars) { ?>
        <p>Aucune wordwar planifiées.</p>
    <?php } ?>
    <ul>
        <?php foreach ($soonwordwars as $wordwar) { ?>
            <li><a href="<?= $wordwar->get_url() ?>">Le <b><?= user_date('d/m/Y à H:i',$wordwar->get_timebegin_timestamp()) ?></b> pour <b><?= $wordwar->get_duration() ?></b>min (par <?= $wordwar->get_creator() ?>)</a></li>
        <?php } ?>
    </ul>
</div>
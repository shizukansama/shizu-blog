<?php

require_once 'config.php';

$action = get_param('action', 'text');

if ($action) {
    switch ($action) {
        case 'create-wordwar':
            $creator  = get_param('creator', 'text');

            $timebegin = get_param('starttime', 'int');
            if(!$timebegin){
                $year     = get_param('year', 'int');
                $month    = get_param('month', 'int');
                $day      = get_param('day', 'int');
                $hour     = get_param('hour', 'int');
                $minute   = get_param('minute', 'int');

                $date = new DateTime($day . '-' . $month . '-' . $year . ' ' . $hour . ':' . $minute, new DateTimeZone(get_user_timezone()));
                $date->setTimezone(new DateTimeZone('UTC'));
                $timebegin = $date->getTimestamp();
            }

            $duration = get_param('duration', 'int');

            // TODO check every value
            $id = add_wordwar_session($creator, $timebegin, $duration);
            if ($id) {
                $wordwars = get_wordwar_session($id);
                add_log('wordwars.php', 'create', 'wordwars\create_session', [
                    'creator'   => $wordwars->get_creator(),
                    'timebegin' => $wordwars->get_timebegin_timestamp(),
                    'duration' => $wordwars->get_duration(),
                ]);
                redirect($wordwars->get_url());
                die;
            }
            // TODO Error !
            break;
    }
}

$participantname = get_ww_name();

// View log
add_log('wordwars.php', 'view', 'wordwars\view_list', ['user' => $participantname, 'ip' => get_ip()]);

// Create word war
$currentyear = date("Y");
$years       = [
    $currentyear,
    $currentyear + 1
];

$months       = [
    1  => "Janvier",
    2  => "Février",
    3  => "Mars",
    4  => "Avril",
    5  => "Mai",
    6  => "Juin",
    7  => "Juillet",
    8  => "Août",
    9  => "Septembre",
    10 => "Octobre",
    11 => "Novembre",
    12 => "Décembre"
];
$days       = 31;

$now = time();
$nextww = get_wordwar_next_time($now);
$secondnextww = get_wordwar_next_time($now, '5');

// WW list
$openwordwars = get_open_wordwar_sessions();
$soonwordwars = get_soon_wordwar_sessions($now);



$title    = 'Word wars';
$template = 'wordwars.php';
require('templates/base_tools.php');

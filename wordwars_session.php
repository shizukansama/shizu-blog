<?php

require_once 'config.php';

$wwid = get_param('wwid', 'int');

if ($wwid) {
    $wwsession = get_wordwar_session($wwid);
}

if (!$wwid || !$wwsession) {
    $hasothercontent = false;

    $title = 'Word wars - Erreur';

    $createwwurl = $CFG->siteUrl . '/wordwars.php';
    $content     = '<div class="error-page"><h2>Word war inconnue</h2>
            <p>La word war que vous recherchez n\'existe pas :(</p>
            <p>Pour en créer une, <a href="' . $createwwurl . '">c\'est par ici !</a></p></div>';
    add_log('wordwars_session.php', 'error', 'wordwars\session_not_exists', ['wwsessionid' => $wwid]);
    require('templates/base.php');
    die;
}

$isdone    = $wwsession->is_done();

$participantname = get_ww_name();
if(!$isdone){
    add_wordwar_participant($wwid, get_ip(), $participantname);
}

add_log('wordwars_session.php', 'view', 'wordwars\view_session', [
    'wwsessionid' => $wwid,
    'participant'     => $participantname,
    "timebegin"   => $wwsession->get_timebegin_timestamp(),
    'duration'    => $wwsession->get_duration()
]);

$bodyclass = " wordwar-launch ";

$metaofdescription = "La word war commence le " . user_date('d/m/Y à H:i', $wwsession->get_timebegin_timestamp()) . " pour " . $wwsession->get_duration() . 'min';

$wwurl = $wwsession->get_url();

$wwparticpants = get_wordwar_participants($wwsession->get_id());

$title    = 'Word wars';
$template = 'wordwars-launch.php';
require('templates/base_tools.php');
